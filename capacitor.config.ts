import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'the-cats-app',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
